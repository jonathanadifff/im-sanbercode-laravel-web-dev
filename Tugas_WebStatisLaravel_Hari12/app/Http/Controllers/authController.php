<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class authController extends Controller
{
   public function registerPage(){
     return view('register');
   }

   public function regist(request $request){
     $namaAwal = $request['nama_awal'];
     $namaAkhir = $request['nama_akhir'];
     $biodata = $request['bio'];

     return view('welcome',['nama_awal' => $namaAwal  , 'nama_akhir' => $namaAkhir  , 'bio' => $biodata ]);
   }
}
