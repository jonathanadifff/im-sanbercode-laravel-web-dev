<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Tugas Laravel Day 12</title>
  </head>
  <body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="post">
      @csrf
      <label>First name</label>
      <br />
      <br />
      <input type="text" name="nama_awal" />
      <br />
      <br />
      <label>Last Name</label>
      <br /><br />
      <input type="text" name="nama_akhir" />
      <br /><br />
      <label>Gender :</label>
      <br />
      <br />
      <div>
        <input type="radio" name="gender" /> Male <br />
        <br />
        <input type="radio" name="gender" /> Female <br />
        <br />
        <input type="radio" name="gender" /> other <br />
        <br />
      </div>

      <label>Nationality :</label>
      <br />
      <br />
      <select name="nationality">
        <option value="indonesia">Indonesia</option>
        <option value="malaysia">Malaysia</option>
        <option value="spanyol">Spanyol</option>
        <option value="thailand">Thailand</option>
        <option value="inggris">Inggris</option>
      </select>
      <br /><br />
      <label>Language Spoken :</label>
      <br />
      <br />
      <input type="checkbox" name="langspoke" /> Bahasa Indonesia
      <br />
      <input type="checkbox" name="langspoke" /> English
      <br />
      <input type="checkbox" name="langspoke" /> Other
      <br />
      <br />
      <label>Bio:</label> <br />
      <br />
      <textarea name="bio" cols="30" rows="10"></textarea>
      <br /><br />
      <button type="submit" value="kirim">Sign Up</button>
    </form>
  </body>
</html>
