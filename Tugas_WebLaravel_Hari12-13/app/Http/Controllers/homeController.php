<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class homeController extends Controller
{

    // controller tugas 12 
    
    public function homePage12(){
        return view('home');
    }

    // controller tugas 13 
    public function homePage(){
        return view('pages.homePage');
    }

    public function tablePage(){
        return view('pages.table');
    }

    public function datatablePage(){
        return view('pages.dataTables');
    }
}
