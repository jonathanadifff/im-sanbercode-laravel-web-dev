<?php

use App\Http\Controllers\authController;
use App\Http\Controllers\homeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// tugas 12 
Route::get('/tugas12', [homeController::class, 'homePage12']);

Route::get('/register', [authController::class, 'registerPage']);

Route::post('/welcome', [authController::class,'regist']);

// end tugas 12 

// tugas 13
Route::get('/', [homeController::class, 'homePage']);
Route::get('/table', [homeController::class, 'tablePage']);
Route::get('/data-table', [homeController::class, 'datatablePage']);
// end tugas 13 