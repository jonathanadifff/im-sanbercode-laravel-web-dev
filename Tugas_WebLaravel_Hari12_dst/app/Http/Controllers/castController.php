<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class castController extends Controller
{
    public function create(){
        return view('pages.cast.tambah');
    }

    public function postCast(request $request){

        $request->validate([
            'nama_cast' => 'required',
            'umur_cast' => 'required',
            'bio_cast' => 'required',
        ]);

        DB::table('cast')->insert([
            'nama' => $request['nama_cast'],
            'umur' => $request['umur_cast'],
            'bio'  => $request['bio_cast']
        ]);
        
        return redirect('/cast');
    }

    public function tampilCast(){
        $cast = DB::table('cast')->get();
        // dd($cast); // untuk nampilin array data
        return view('pages.cast.tampil', ['cast' => $cast]);
    }

    public function detailCast($id){
        $cast = DB::table('cast')->where('id', $id)->first();
        // dd($cast); 
        return view('pages.cast.detail', ['cast' => $cast]);
    }

    public function editCast($id){
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('pages.cast.edit', ['cast' => $cast]);
    }

    public function updateCast(request $request,$id){

        $request->validate([
            'nama_cast' => 'required',
            'umur_cast' => 'required',
            'bio_cast' => 'required',
        ]);

         DB::table('cast')
         ->where('id', $id)
         ->update(['nama' => $request['nama_cast'],
                   'umur' => $request['umur_cast'],
                    'bio' => $request['bio_cast']]);

         return redirect('/cast');           
    }

    public function deleteCast($id){
       DB::table('cast')
       ->where('id', $id)
       ->delete();

       return redirect('/cast');
    }
}
