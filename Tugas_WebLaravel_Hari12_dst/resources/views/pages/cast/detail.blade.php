@extends('layout.master')

@section('judul')
    Detail Cast {{$cast->nama}}
@endsection


@section('content')
    <h1>{{$cast->nama}}</h1>
    <h2>Age: {{$cast->umur}}</h2>
    <p>{{$cast->bio}}</p>

    <a
        class="btn btn-secondary"
        href="/cast"
        role="button"
        >Back</a
    >
    
@endsection