@extends('layout.master')

@section('judul')
    Edit Data Cast {{$cast->nama}}
@endsection

@section('content')
<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label for="inputNama">Nama</label>
        <input name="nama_cast" type="text" class="form-control" id="inputNama" placeholder="Masukkan Nama Anda!" value="{{$cast->nama}} "> 
    </div>
    @error('nama_cast')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label for="inputUmur">Umur</label>
        <input name="umur_cast" type="text" class="form-control" id="inputUmur" placeholder="Masukkan Umur Anda!" value="{{$cast->umur}} "> 
    </div>
    @error('umur_cast')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label for="inputBio">Bio</label> <br>
        <textarea name="bio_cast" class="form-control" id="inputBio" cols="30" rows="10" placeholder="Masukkan Biodata Anda!">{{$cast->bio}}</textarea>
    </div>
    @error('bio_cast')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <button class="btn btn-secondary" href="/cast">Back</button>
    <button type="submit" class="btn btn-primary">Edit</button>
</form>
@endsection