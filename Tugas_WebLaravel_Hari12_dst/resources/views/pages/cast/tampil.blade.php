@extends('layout.master')

@section('judul')
   List Cast
@endsection

@section('content')

<table class="table mb-2">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Nama</th>
            <th scope="col">Action</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key => $value)
        <tr>
            <td>{{$key + 1}}</td>
            <td>{{$value -> nama}}</td>
            <td>
              

                <form action="/cast/{{$value->id}}" method="POST">
                @csrf
                @method('DELETE')

                <a
                class="btn btn-primary"
                href="/cast/{{$value->id}}"
                role="button"
                >Detail
                </a>

                <a
                class="btn btn-warning"
                href="/cast/{{$value->id}}/edit"
                role="button"
                >Edit
                </a>

                <button type="submit" class="btn btn-danger">Delete</button>
                </form>
        

            </td>
        </tr>
        @empty
        <tr>
            <td>No Data</td>
        </tr>
        @endforelse
    </tbody>
</table>

    
 <a
    class="btn btn-primary"
    href="/cast/create"
    role="button"
    >Add Cast</a
 >
 
@endsection