@extends('layout.master')

@section('judul')
   Halaman Tambah Data Cast  
@endsection

@section('content')
<form action="/cast" method="POST">
    @csrf
    @method('POST')
    <div class="form-group">
        <label for="inputNama">Nama</label>
        <input name="nama_cast" type="text" class="form-control" id="inputNama" placeholder="Masukkan Nama Anda!">
    </div>
    @error('nama_cast')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label for="inputUmur">Umur</label>
        <input name="umur_cast" type="text" class="form-control" id="inputUmur" placeholder="Masukkan Umur Anda!">
    </div>
    @error('umur_cast')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label for="inputBio">Bio</label> <br>
        <textarea name="bio_cast" class="form-control" id="inputBio" cols="30" rows="10" placeholder="Masukkan Biodata Anda!"></textarea>
    </div>
    @error('bio_cast')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <a href="/cast" class="btn btn-secondary">Back</a>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection