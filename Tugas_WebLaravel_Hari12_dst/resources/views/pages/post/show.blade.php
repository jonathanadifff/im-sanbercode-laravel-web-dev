@extends('layout.master')

@section('judul')
    Detail Post {{$post->title}}
@endsection

@section('content')
<h2>Show Post {{$post->id}}</h2>
<h4>{{$post->title}}</h4>
<p>{{$post->body}}</p>

<a href="/post" class="btn btn-secondary my-2">Back</a>
@endsection