<?php

use App\Http\Controllers\authController;
use App\Http\Controllers\castController;
use App\Http\Controllers\homeController;
use App\Http\Controllers\postController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// tugas 12 
Route::get('/tugas12', [homeController::class, 'homePage12']);

Route::get('/register', [authController::class, 'registerPage']);

Route::post('/welcome', [authController::class,'regist']);

// end tugas 12 

// tugas 13
Route::get('/', [homeController::class, 'homePage']);
Route::get('/table', [homeController::class, 'tablePage']);
Route::get('/data-table', [homeController::class, 'datatablePage']);
// end tugas 13 

// tugas 15

// laman tambah cast 
Route::get('/cast/create', [castController::class, 'create']);
//post data ke database cast
Route::post('/cast',[castController::class,'postCast']);

// panggil data dari database cast 
Route::get('/cast',[castController::class, 'tampilCast']);
// panggil data detail cast dari database
Route::get('/cast/{cast_id}', [castController::class, 'detailCast']);

//tampilin laman edit data cast 
Route::get('/cast/{cast_id}/edit', [castController::class, 'editCast']);
//fungsi edit data
Route::put('/cast/{cast_id}', [castController::class, 'updateCast']);

// fungsi delete data
Route::delete('/cast/{cast_id}',[castController::class, 'deleteCast']);

// end tugas 15

// start CRUD with eloquent ORM
Route::resource('post', postController::class);
// end CRUD with eloquent ORM